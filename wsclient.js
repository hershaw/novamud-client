var webSocket   = null;

if (window.location.protocol != 'http:' && window.location.href.indexOf('file://') == -1)
{
    window.location = 'http:' + window.location.href.substring(window.location.protocol.length);
}

/**
 * Event handler for clicking on button "Connect"
 */
function onConnectClick() {
    var connstring = document.getElementById("connstring").value;
    openWSConnection(connstring);
}
/**
 * Event handler for clicking on button "Disconnect"
 */
function onDisconnectClick() {
    webSocket.close();
}
/**
 * Open a new WebSocket connection using the given parameters
 */
function openWSConnection(webSocketURL) {
    console.log("openWSConnection::Connecting to: " + webSocketURL);
    var textarea = document.getElementById('incomingMsgOutput');

    try {
        var connBtn = document.getElementById("btnConnect");
        var disconnBtn = document.getElementById("btnDisconnect");
        var isOpen = false;
        webSocket = new WebSocket(webSocketURL);

        webSocket.onopen = function(openEvent) {
            console.log("WebSocket OPEN: " + JSON.stringify(openEvent, null, 4));
            // document.getElementById("btnSend").disabled       = false;
            connBtn.disabled    = true;
            disconnBtn.disabled = false;
            textarea.value = '';
            isOpen = true;
            keepAlive();
        };
        webSocket.onclose = function (closeEvent) {
            console.log("WebSocket CLOSE: " + JSON.stringify(closeEvent, null, 4));
            // document.getElementById("btnSend").disabled       = true;
            connBtn.disabled    = false;
            disconnBtn.disabled = true;
            cancelKeepAlive();
        };
        webSocket.onerror = function (errorEvent) {
            console.log("WebSocket ERROR: " + JSON.stringify(errorEvent, null, 4));
        };
        webSocket.onmessage = function (messageEvent) {
            var wsMsg = messageEvent.data;
            console.log("WebSocket MESSAGE: " + wsMsg);
            if (wsMsg.indexOf("error") > 0) {
                textarea.value += "error: " + wsMsg.error + "\r\n";
            } else {
                textarea.value += wsMsg + "\r\n";
            }
            textarea.scrollTop = textarea.scrollHeight;
        };
        var timerId = 0;
        function keepAlive() {
            var timeout = 9000;
            if (isOpen) {
                webSocket.send('');
            }
            timerId = setTimeout(keepAlive, timeout);
        }
        function cancelKeepAlive() {
            isOpen = false;
            if (timerId) {
                clearTimeout(timerId);
            }
}
    } catch (exception) {
        console.error(exception);
    }
}

function writeMsg(msg) {
    var textarea = document.getElementById('incomingMsgOutput');
    textarea.value += msg + "\r\n";
}
/**
 * Send a message to the WebSocket server
 */
function onSendClick() {
    if (webSocket.readyState != WebSocket.OPEN) {
        console.error("webSocket is not open: " + webSocket.readyState);
        writeMsg('The server does not seem to be running...');
        return;
    }
    var msg = document.getElementById("message").value;
    webSocket.send(msg);
}

function onKeypress(event) {
    if (event.keyCode == 13) {
        onSendClick();
        document.getElementById("message").value = '';
    }
}
